\documentclass{article}
\usepackage[utf8]{inputenc}

\title{Topology Notes}
\author{Jad Elkhaleq Ghalayini}
\date{May 30 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}

\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{notation}{Notation}
\newtheorem*{corollary}{Corollary}
\newtheorem{proposition}{Proposition}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\topo}[0]{\mathcal{T}}
\newcommand{\base}[0]{\mathcal{B}}
\newcommand{\mc}[1]{\mathcal{#1}}


\begin{document}

\maketitle

\section*{Recall}
\begin{definition}
In \((X, \topo), x \in X, \base_x \subseteq \topo\) is called a \underline{local basis at \(x\)} if \(\forall U \in \topo, x \in U \implies \exists B \in \base_x\) such that \(x \in B \subseteq U\)
\end{definition}
\begin{definition}
\((X, \topo)\) is \underline{first countable} if every point in \(X\) has a countable basis
\end{definition}
The only non first countable spaces we have are \((X, \topo_{\text{co-finite}})\) and \((X, \topo_{\text{co-countable}})\) when \(X\) is uncountable.
\subsection*{Facts:}
\begin{itemize}
    \item Let \(A \subseteq X, \{x_n\} \subseteq A\). If \(x_n \to x\), then \(x \in \bar{A}\).
    \item If \((X, \topo)\) is \(T_2\), then each sequence has \(\leq 1\) limit point.
\end{itemize}
These two implications both reverse if \((X, \topo)\) is first countable. The topological properties we now have are:
\begin{itemize}
    \item \(T_0, T_1, T_2\)
    \item Separable
    \item First and second countable
    \item Countable chain condition
\end{itemize}
\begin{proposition}
\(\reals_{\text{co-countable}}\) is not first countable.
\end{proposition}
\begin{proof}
We'll show \(0\) has no countable local basis. Let \(\base_x = \{B_n : n \in \nats\}\) be any countable collection of open sets each containing 0. We want to show that \(\base_x\) is not a local basis at 0.

Since each \(B_n\) is co-countable, let \(B_n = \reals\setminus C_n\) where \(C_n\) is countable. Let
\[C = \bigcup_{n = 1}^\infty C_n\]
Let \(x \in \reals\setminus C\), and let \(C' = C \cup \{x\}\), which is clearly countable. We can hence construct the open set \(U = \reals\setminus C'\)
But \(\not\exists B_n\) such that \(0 \in B_n \subseteq U\).
\end{proof}

\section*{Continuity}
\begin{definition}
Let \((X, \topo)\) and \((Y, \mc{U})\) be topological spaces. A function \(f: X \to Y\) is \underline{continuous} if 
\[\forall U \in \mc{U}, f^{-1}(U) = \{x \in X : f(x) \in U\} \in \topo\] 
We write this
\[f: (X, \topo) \to (Y, \mc{U})\]
\end{definition}
Examples
\begin{enumerate}
    
    \item \(x^3: \reals_{\text{usual}} \to \reals_{\text{usual}}\) is continuous. 
    \begin{proof}
    Call it \(f\). Fix \(U \in \topo_{\text{usual}}\). We want to show that \(f^{-1}(U)\) is open.
    
    Fix any \(x \in f^{-1}(U)\). Then \(\exists\epsilon > 0\) such that \((f(x) - \epsilon, f(x) + \epsilon) \subseteq U\). Since \(f\) is \(\epsilon-\delta\) continuous, \(\exists \delta > 0\) such that
    \[y \in (x - \delta, x + \delta) \implies f(y) \in (f(x) - \epsilon, f(x) + \epsilon) \subseteq U\]
    So
    \[0 < |y - x| < \delta \implies |f(y) - f(x)| < \epsilon\]
    Then \(\forall y \in (x - \delta, x + \delta), f(y) \in U\), so \((x - \delta, x + \delta) \subseteq f^{-1}(U)\). 
    \end{proof}

    \item \(f_a: \reals^n_{\text{usual}} \to \reals_{\text{usual}}\) given by \(f_a(x) = d(x, a)\) for any fixed \(a\) is continuous
    \item \(\pi_1: \reals^2_{\text{usual}} \to \reals_{\text{usual}}\) given \(\pi_1(x, y) = x\) is continuous
    \item Any function whose domain is a discrete space
    \item Any function whose co-domain is indiscrete
    \item Any constant function \(f: X \to Y, f(x) = c \forall x\), for some \(c \in Y\).
    \begin{proof}
    \(\forall A \subseteq y\), if \(c \in A, f^{-1}(A) = X\). If \(c \notin A, f^{-1}(A) = \varnothing\).
    \end{proof}
    \item Compositions of continuous functions are continuous
    \item \(id: X \to X, id(X) = Y\) is continuous
    \begin{proposition}
    \(id: (X, \topo_1) \to (X, \topo_2)\) is continuous \(\iff\) \(\topo_1\) refines \(\topo_2\).
    \end{proposition}

\end{enumerate}
\section*{Equivalent Characterizations}
\begin{proposition}
Let \((X, \topo), (Y, \mc{U})\) be topological spaces, \(\base\) a basis on \(Y\) and \(\mc{S}\) a sub-basis on \(Y\), both generating \(\mc{U}\). Let \(f: X \to Y\) be a function. The following are equivalent:
\begin{itemize}
    \item \(f\) is continuous
    \item \(\forall B \in \base, f^{-1}(B) \in \topo\)
    \item \(\forall S \in \mc{S}, f^{-1}(S) \in \topo\)
\end{itemize}
\end{proposition}
\begin{proof}
Exercise.
\end{proof}
\begin{definition}
Let \((X, \topo)\) and \((Y, \mc{U})\) be topological spaces, and let \(x \in X\). \(f: X \to Y\) is \underline{continuous at \(x\)} if \(\forall V \in \mc{U}\) containing \(f(x)\), \(\exists U \in \topo\) containing \(x\) such that \(f(U) \subseteq V\).
\end{definition}
\begin{proposition}
Let \((X, \topo), (Y, \topo)\) be topological spaces and \(f: X \to Y\) be a function.
The following are equivalent:
\begin{enumerate}
    \item \(f\) is continuous
    \item \(\forall x \in X\), \(f\) is continuous at \(x\)
    \item \(\forall C \subseteq Y\) closed, \(f^{-1}(C)\) \underline{is closed} in \(X\)
    \item \(\forall A \subseteq X\), \(f(\bar{A}) \subseteq \overline{f(A)}\)
\end{enumerate}
\end{proposition}
\begin{proof}
In notes: \(2 \implies 1 \implies 4 \implies 3 \implies 2\). Here: \(4 \implies 3\):

Fix \(C \subseteq Y\) closed. We want to show \(f^{-1}(C) \subseteq X\) is closed. Let \(A = f^{-1}(C)\). Want to show \(A\) is closed \(\iff A = \bar{A} \iff \bar{A} \subseteq A\).
Then \[f(x) \in \overline{f(A)} \subseteq \bar{C} = C\]
since \(C\) is closed. i.e. \(x \in f^{-1}(C) = A\).
\[\overline{f(A)} = \overline{f(f^{-1}(C))} \subseteq \bar{C}\]
\end{proof}
\section*{Open and Closed Functions}
\begin{definition}
If \(f: X \to Y\),
\begin{enumerate}
    \item \(f\) is \underline{open} if \(f(U)\) is open \(\forall U\) open in \(X\)
    \item \(f\) is \underline{closed} if \(f(C)\) is \underline{closed} \(\forall C \subseteq X\) closed. 
\end{enumerate}
\end{definition}
Just like with open and closed sets, open is \textit{not} the opposite of closed.

Examples:
\begin{enumerate}
    \item \(f: \reals_{\text{usual}} \to \reals_{\text{usual}}\), \(x \mapsto 7\) is closed, not open
    \item \(\pi_1 : \reals^2_{\text{usual}} \to \reals_{\text{usual}}\), \((x, y) \mapsto x\) is open, not closed (exercise)
    \item \(f: X \to Y_{\text{discrete}}\) is open \textit{and} closed
    \item \(f: \reals_{\text{Sorg}} \to \reals_{\text{Sorg}}\), \(x \mapsto |x|\) is not open or closed:
    \[f([-7, -1)) = (1, 7]\]
    which is not open or closed.
\end{enumerate}
\begin{proposition}
If \(f: X \to Y\) is bijective, then it's open \(\iff\) it's closed
\end{proposition}
\section*{Homeomorphisms}
\begin{definition}
Let \((X, \topo)\) and \((Y, \mc{U})\) be topological spaces, and \(f: X \to Y\) be a bijection. \(f\) is a \underline{homeomorphism} if \(f\) and \(f^{-1}\) are continuous. In this case, \((X, \topo)\) and \((Y, \mc{U})\) are \underline{homeomorphic} denoted \((X, \topo) \simeq (Y, \mc{U})\).
\end{definition}
\begin{proposition}
The following are equivalent for a bijection \(f: X \to Y\):
\begin{enumerate}
    \item \(f\) is a homeo (homeomorphism)
    \item \(f\) is continuous and open
    \item \(f\) is continuous and closed
    \item \(\forall U \in \mc{P}(X), U \in \topo \iff f(U) \in \mc{U}\)
\end{enumerate}
\end{proposition}
Examples:
\begin{itemize}
    \item Any non-constant affine linear function \(f: \reals_{\text{usual}} \to \reals_{\text{usual}}\) is a homeomorphism
    \item \(f: (X, \topo_p) \to (X, \topo_q)\), \(p \neq q\) is a homeomorphism if and only if it's a bijection such that \(f(p) = q\)
    \item \(f: X_{\text{discrete}} \to Y_{\text{discrete}}\) is a homeomorphism if and only if it's a bijection
    \item \(\tan(-pi/2, \pi/2) \to \reals_{\text{usual}}\) is a homeomorphism
    \item \(\not\exists\) homeomorphisms \(f: (\rationals, \topo) \to (\reals, \mc{U})\).
\end{itemize}
\end{document}
\begin{definition}
A property \(\phi\) of topological spaces is a \underline{topological invariant} if whenever \((X, \topo)\) has \(\phi\) and \((X, \topo) \simeq (Y, \mc{U})\), then \((Y, \mc{U})\) has that property.
\end{definition}