\documentclass{article}
\usepackage[utf8]{inputenc}

\title{Topology Notes}
\author{Jad Elkhaleq Ghalayini}
\date{May 9 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}

\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\topo}[0]{\mathcal{T}}

\begin{document}

\maketitle

Topology deals with what things are open sets in some space. You are used to this concept from at least one example: open intervals on \(\reals\). The topology we work on tells us which sets are open. We will be dealing with many different topologies on \(\reals\), so for us \((a, b)\) and \([a,b]\) are just notation for intervals, and may not be open (or closed) in the \textit{topological} sense. Many of you may also be familiar with open and closed sets on \(\reals^n\).

Another notion you must know well is that of a continuous function.
\begin{definition}
\(f: \reals^n \to \reals\) is continuous at \(a \in \reals^n\) if
\[\forall \epsilon > 0, \exists \delta > 0, 0 < ||x - a|| < \delta \implies |f(x) - f(a)| < \epsilon\]
\end{definition}
This definition is very specific to \(\reals^n\): we are assuming that we can find distances between elements of \(\reals^n\), \(\real\). You are expected to have seen this in at least one context, if only in \(\reals\). We will substantially generalize the definition of continuity.

Another idea we will talk about very much is sequence convergence.
\begin{definition}
If we have a sequence \(\{x_n\}_{n = 1}^\infty \subseteq \reals^n\), we say ``\(x_n \to x\)'' (the sequence converges to \(x\)) if
\[\forall \epsilon > 0, \exists N \in \reals, \forall n \in \nats, n > N \implies ||x_n - x|| < \epsilon\]
\end{definition}
If \(x_n \to x\) and \(f: \reals \to \reals\) is continuous, then
\[f'(x_n) \to f(x)\]
Does this reverse? The answer is yes: if we have a function where this holds for every continuous sequence in \(\reals\), then the function must be continuous. However, this is not true in all spaces, which we will get to later.

Two more sort of general, vague things are the ideas of
\begin{itemize}
    
    \item ``Closeness:'' what does it mean for points in a space to be ``close'' to each other? The answer is: it depends on our topology. We can ask about what it means for two sets to be close to each other as well. Are the sets \((1, 2), (2, 3)\) close, for example. What about \((1, 2), [2, 3)\)?
    
    \item ``Niceness'' of subsets of a space. When you come into first year calculus, you learn about a hiearchy of ``niceness'' for functions. First, you start with functions. They can be crazy, like the popcorn function, and do pretty much anything. Then you learn about functions with limits at some points, then continuous functions, then differentiable functions, and then \(k\)-times differentiable functions, giving you the \(C\)-hierarchy,
    \[\mathcal{C}^1,...,\mathcal{C}^k,...,\mathcal{C}^\infty\]
    ending with infinitely differentiable functions. Finally, you have analytic functions.
    
    The intermediate value theorem and extreme value theorem work if we have a continuous function on a \textit{closed} interval of \(\reals\). It's a quirk of \(\reals\) that the conditions used for both are the same. The IVT really works of \textit{connectedness}, which we will discuss later, whereas the EVT works off the basis of \textit{denseness}. 
    
    Another thing you might have learned is that if you have a vector field \(F: \reals^3 \to \reals^3\), \(F\) is conservative \(\implies\) \(\nabla\times F = 0\). A natural question to ask is whether the converse of this is true, and the answer to this is yes if the function is defined over a \textit{simply connected set}. Overall, the idea is that we can assign various flags of ``niceness'' to various sets which matter quite a bit in determining their properties.
    
\end{itemize}

Let me just remind you about what you know about open sets of \(\reals^n\). Some of you may not have formally studied \(\reals^n\), so I'll try to give a translation into just \(\reals\).

A topology is a collection of sets that we call ``open''. A topological space is a space along with the set of open points. We're about to formalize this. For everything I'm about to say, we'll have
\[x = (x_1,...,x_n) \in \reals^n, y = (y_1,...,y_n) \in \reals^n\]
To get everyone on the same page, we have the ``ball'' of radius \(\epsilon\) centered at \(x\) not including the boundary
\[B_\epsilon(x) = \{y \in \reals^n : ||x - y|| < \epsilon\}\]
In \(\reals\), this would just be \((x - \epsilon, x + \epsilon)\).

\begin{definition}
In \(\reals^n\), \(U \subseteq \reals^n\) is \underline{open} if
\[\forall x \in U, \exists \epsilon > 0, B_\epsilon(x) \subseteq U\]
\end{definition}

\subsection*{Properties of open sets in \(\reals^n\)}

There are a few properties that are simple, but stand out as especially useful, in proving a variety of theorems using the previous definition of openness:
\begin{enumerate}
    \item \(\reals^n\) is open, and \(\varnothing\) is open
    \item Any union of open sets is open
    \item A finite intersection of open sets is open
\end{enumerate}
These three properties will kind of jump out atyou if you try to prove anything about open sets, and it just so happens that these properties were what was chosen in trying to define and generalize what open is. Here's now the official definition of topology:
\begin{definition}
Let \(X\) be a set. A collection \(\topo \subseteq \mathcal{P}(x)\) is a \underline{topology on \(X\)} if:
\begin{enumerate}
    \item \(X, \varnothing \in \topo\)
    \item \(\topo\) is closed under finite intersections, i.e.
    \[\forall U_1,...,U_n \in \topo, U_1 \cap ... \cap U_n \in \topo\]
    \item \(\topo\) is closed under all unions, i.e. if 
    \[\{U_\alpha : \alpha \in I\} \subseteq \topo\]
    then
    \[\bigcup_{\alpha \in I}U_\alpha \in \topo\]
\end{enumerate}
Given such a \(\topo\), \((X, \topo)\) is \underline{a topological state}. Elements of \(\topo\) are \underline{open sets}. 
\end{definition}
Note: \(\mathcal{P}(X)\) is the set of all subsets of \(X\).

We're gonna list off some topologies. In some cases I'll explain why these are topoliges, in others I'll leave the proof to you on the Big List.
On \(X = \reals^n\), 
\[\topo_{\text{usual}} = \{U \subseteq \reals^n: \forall x \in U, \exists \epsilon > 0, B_{\epsilon}(x) \subseteq U\}\]
On \(\reals\), this would look like
\[\{U \subseteq \reals: \forall x \in U, \exists \epsilon > 0, (x - \epsilon, x + \epsilon) \subseteq U\}\]
The problem with \(\reals^n\) is that it is too nice. If you could list out the list of nice things a topology could have, \(\reals^n\) has them all. This can make it difficult to separate some of the interesting properties a topology can have. Now, we will look at some more interesting topologies. Fix any \(X \neq \varnothing\)

\begin{enumerate}

    \item \(\topo_{\text{discrete}} = \mathcal{P}(x)\). Note that this topology works on \textit{any} set, whereas the above topology on \(\reals^n\) requires a lot of structure, for example, distances between elements. This can come up somewhat often, for example, when assigning topologies for function spaces. This is the biggest possible topology, but what about the smallest:
    
    \item \(\topo_{\text{indiscrete}} = \{X, \varnothing\}\). No one uses this topology, because it doesn't do anything useful. One interesting this it does, however, is make \textit{every} sequence converge to every real number. The discrete topology also does soomething interesting: all function whose domains are discrete spaces are continuous, and the only sequences that converge in the discrete space end up as constants.
    
\end{enumerate}

If we have a 3 point space, with \(X = \{x, y, z\}\), then \(\topo = \{X, \varnothing, \{x, y\}, \{y, z\}, \{y\}\}\) is a topology. You could add things to this, like \(\{z\}\), for example, and still have a topology.

On \(\reals\),
\[\topo_{\text{ray}} = \{(a, \infty) : a \in \reals\} \cup \{\varnothing, \reals\}\]
is a topology. This has some interesting properties, for example, \(\left\{-\frac{1}{n}\right\}\) converges to any negative number, whereas \(\left\{\frac{1}{n}\right\}\) converges only to 0. We haven't, of course, defined convergence, yet, but the point is to show you that things can be weird.

Let \(X\) be a set. Then
\begin{enumerate}
    \item \(\topo_{\text{co-finite}} = \{U \subseteq X : X \setminus U \text{is finite}\} \cup \{\varnothing\}\)
    \item \(\topo_{\text{co-countable}} = \{U \subseteq X : X \setminus U \text{is countable}\} \cup \{\varnothing\}\) 
\end{enumerate}
are topologies.
Fix, \(p \in X\). Then
\[\topo_p = \{U \subseteq X : p \in U\} \cup \{\varnothing\}\]
is a topology.

\end{document}
