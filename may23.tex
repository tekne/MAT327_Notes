\documentclass{article}
\usepackage[utf8]{inputenc}

\title{Topology Notes}
\author{Jad Elkhaleq Ghalayini}
\date{May 23 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}

\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{notation}{Notation}
\newtheorem*{corollary}{Corollary}
\newtheorem{proposition}{Proposition}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\topo}[0]{\mathcal{T}}
\newcommand{\base}[0]{\mathcal{B}}
\newcommand{\mc}[1]{\mathcal{#1}}


\begin{document}

\maketitle

\section{Things we know about countability}
\begin{definition}
A set \(A\) is \underline{countable} if \(|A| \leq |N|\). In particular \(A\) is countably infinite if there exists a bijection \(f : \nats \to A\)
\end{definition}
Facts: \begin{itemize}
    \item All subsets of countable sets are countable.
    \item Countable unions of countable sets are countable
    \item \(\ints, \rationals\) are countable
    \item Any \underline{finite} Cartesian product of countable sets is countable
\end{itemize}
\begin{definition}
A set that is not countable is called \underline{uncountable}.
\end{definition}
\begin{proposition}
If \(A\) is uncountable and \(f: A \to B\) is a bijection, then \(B\) is also uncountable
\end{proposition}
\begin{proof}
Assume not. We split our proof into two cases:
\begin{itemize}
    \item Assume \(B\) is finite. Then, \(A\) must be finite, which contradicts the fact that \(A\) is uncountable.
    \item Assume \(B\) is countably infinite. Then there exists a bijection \(g: B \to \nats\), implying that \(h = f \circ g\) is a bijection from \(A\) to \(\nats\), implying that \(A\) is countable, which is a contradiction.
\end{itemize}
\end{proof}
\begin{theorem}[Cantor]
There is no surjection \(f: \nats \to (0, 1)\). In particular, \((0, 1)\) is uncountable.
\end{theorem}
\begin{proof}
Fix a function \(f: \nats \to (0, 1)\). We want to show that \(f\) is not a surjection.

Elements of \((0, 1)\) have decimal expansions, i.e. if \(a \in (0, 1)\), \(a = 0.a_1a_2a_3a_4...\) (where \(a_i \in \{0,1,...,9\}\)). If \(a\) has a finite decimal expansion, just imagine all terms afterwards are \(0\).

For each \(n \in \nats\), let \(f(n) = 0.x_1^nx_2^nx_3^nx_4^n...\). In this way we form an infinite ``matrix''
\[\begin{array}{ccc} x_1^1 & x_2^1 & \dots \\ x_1^2 & x_2^2 & \dots \\ \vdots & \vdots & \ddots \end{array}\]
We need to show that there is some real number that cannot possibly be on the list. The way to do that is pretty easy: since there are countably many rows in the list, construct a new real number \(y = 0.y_1y_2y_3...\) such that
\[\forall n \in \nats, y_n \neq x_n^n\]
To do this properly, you should come up with a rule, e.g. incrementing the number mod 10. We hence have that \(y\) is not equal to each element in the list, and hence that \(y\) is not in the list, implying \(f\) is not surjective.
\end{proof}
Diagonalization is a very general technique for ``avoiding'' an infinite list of things.
\begin{corollary}
\(\reals\) is uncountable and has the same cardinality as \((0, 1)\)
\end{corollary}
\begin{proof}
Consider the bijection \(h: (0, 1) \to (-\pi/2, \pi/2)\) such that \(x \mapsto \pi x - \frac{\pi}{2}\).
\[\tan: (-\pi/2, \pi/2) \to \reals\]
is a bijection.
\end{proof}
Question: is there a set \(A \subseteq \reals\) such that \(|N| < |A| < |\reals|\). The question of whether this is true or not is called the continuum hypothesis. This is not a question that is possible to answer: as in, we proved this question is unanswerable. It's possible to construct models of the axioms where there are no such sets, and models where there are. This is a natural question to ask, and yet immediately goes into very weird territory.

\begin{theorem}[Cantor]
For any set \(A\), \(|A| < |\mathcal{P}(A)|\).
\end{theorem}
\begin{proof}
Fix an infinite set \(A\). We want to show that \(\not\exists\) a surjection \(A \to \mathcal{P}(A)\).

Fix a function \(f: A \to \mathcal{P}(A)\),
\[f: a \mapsto X, a \in A, X \subseteq A\]
Let \(D = \{a \in A : a \neq f(a)\}\). \(D\) is a subset of \(A\), so \(D \in \mathcal{P}(A)\).

Assume for the sake of contradiction that \(f\) is surjective, so \(D = f(b)\) for some \(b \in A\).

Question: is \(b \in D = f(b)\)? The answer, surely, is either yes or no. Let's examine both cases:
\begin{itemize}
    \item Yes: assume \(b \in D = f(b)\). So by definition \(b \notin f(b) = D\), leading to a contradiction.
    \item No: assume \(b \notin D = f(b)\). So by definition \(b \in D = f(b)\), leading to a contradiction.
\end{itemize}

So \(f\) cannot have been surjective.
\end{proof}
Note that this tells us that
\[|A| < |\mathcal{P}(A)| < |\mathcal{P}(\mathcal{P}(A))| < ...\]
showing that we have at least countably infintitely many sizes of infinity. And we can go further...

\section{Back to Topology}
\begin{definition}
A topological space \((X, \topo)\) is \underline{separable} if it has a countable, dense subset.
\end{definition}
\((R, \topo_{\text{usual}})\) is an extremely complicated object, with \(\topo_{\text{usual}}\) having an even larger cardinaity than \(\reals\). But \(\base = \{(a, b) : a < b \in \rationals\}\) is a basis for it and is clearly countable (there is a surjection \(\rationals \times \rationals \to \base\)). So all the information in \(\topo_{\text{usual}}\) can be summarized in this countable set. This hints at an important property:
\begin{definition}
A topological space \((X, \topo)\) is \underline{second countable} if there is a countable basis for \(\topo\).
\end{definition}
\begin{definition}
A topological space \(X, \topo)\) has \underline{the countable chain condition} (ccc) if there are no uncountable collections of mutually-disjoint open sets. 
\end{definition}
The only set we can readily prove has the CCC for now is \(\reals_{\text{usual}}\). It's a good question to ask whether the Sorgenfrey line is second-countable. Note that if it is second countable, something weird is going to have to happen, since
\[\{[a, b) : a < b \in \rationals\}\]
is \textit{not} a basis for the Sorgenfrey line.

Recall from the Big List that if \(A \subseteq \reals^n\) then 
\[x \in \bar{A} \iff \exists \{x_n\} \subseteq A, x_n \to x\]
\begin{definition}
Let \((X, \topo)\) be a topological space, \(x \in X\) and \(\{x_n\}_{n \in \nats}\) a sequence in \(X\). 
We say \text{\(\{x_n\}\) converges to \(X\)} if 
\[\forall U \in \topo, x \in U \implies [\exists N \in \nats, \forall n \in \nats, (n > N \implies x_n \in U)]\]
\end{definition}

Examples: 
\begin{itemize}

    \item In \(\reals_{\text{usual}}\) and the Sorgenfrey-line, \(\frac{1}{n} \to 0\). In the Sorgenfrey-line, however, \(-\frac{1}{n} \not\to 0\).
    
    \item Every eventually-constant sequence converges to the constant value in any space.
    
    \item In \((X, \topo_{\text{discrete}})\) no other sequences converge.
    
    \item In \((X, \topo_p)\), if \(x \in X\), then \(\{x, p\}\). Also a sequence converging to \(p\) converges to everything.
    
    \item In \((X, \topo_{\text{indiscrete}})\) all sequences converge to everything. This topology is the worst at everything, but especially that.
    
    \item In \((\reals, \topo_{\text{ray}})\), \(\frac{1}{n} \to 0\), but also \(\frac{1}{n} \to -10\). Also, any sequence of positive numbers converges to anything negative.
    
    This is why we need to prove that sequences do not necessarily have unique limits. Sequences tend not to be very useful unless they have unique limits.
    
    \item In \((\reals, \topo_{\text{co-finite}})\), \(0, 1, 2, 3, 3, 3...\) converges to 3, but \(0,1,0,1,0,1,...\) doesn't converge. But \(1, 2, 3, 4, 5, 6, 7, ...\) converges to everything, since the sequence takes on infinitely many values. The set of values of the sequence is dense.
    
    \item In \((\reals, \topo_{\text{co-countable}}\),
    \[\forall {x_n}_{n \in \nats} \subseteq \reals, \reals \setminus \{x_n : n \in \nats\}\]
    is open. If \(\{x_n\}\) is eventually constant \(x\), then \(x_n \to x\). No other sequences, however, converge.
    
\end{itemize}

\begin{definition}
\((X, \topo)\) is called \underline{\(T_0\)} (or sometimes \underline{Kolmogorov}) if 
\(\forall x \neq y \in X, \exists U \in \topo\) such that \(U\) contains one of \(x\) or \(y\) and not the other.
\end{definition}
\begin{definition}
\((X, \topo)\) is called \underline{\(T_1\)} (or sometimes \underline{Frechet}) if 
\[\forall x \neq y \in X \exists U, V \in \topo, x \in U \not\ni y, y \in V \not\ni x\]
\end{definition}
This is a symmetrized version of \(T_0\). An example of a set which is \(T_0\) but not \(T_1\) is \(\topo_p\): if we choose \(y = p\) and arbitrary \(x \neq p\), we can always construct the open singleton \(\{p\}\) containing \(p\), but any open set containing \(x\) also contains \(p\).

Throughout the course, we will construct the ``\(T\)-hierarchy'' of such properties. Now, though, a question: does what we have so far help ``fix'' sequences?
\begin{proposition}
If \((X, \topo)\) is \(T_1\) and \(x \in X\), then the sequence \(x_1x_1x_1...\) converges to \(x\) \underline{and nothing else.}
\end{proposition}
\begin{proof}
Exercise.
\end{proof}
\begin{proposition}
Let \((X, \topo)\) be a topological space. The following are equivalent:
\begin{enumerate}
    \item \((X, \topo)\) is \(T_1\)
    \item \(\forall x \in X, \{x\}\) is closed
    \item Finite sets are closed
    \item \(\forall A \subseteq X, A = \bigcap\{U \subseteq X : U \in \topo, A \subseteq U\}\).
\end{enumerate}
\end{proposition}
Example: \((\reals, \topo_{\text{co-finite}}\) is \(T_1\).
\begin{proof}
Given \(x \neq y\), choose \(U = \reals \setminus \{y\}, V = \reals \setminus \{x\}\).
\end{proof}
Recall \(1, 2, 3, 4, 5,...\) converges to \underline{all} points. So \(T_1\) is definitely not enough to ensure sequences are useful. So how do we make sequences only converge to unique points?
\begin{definition}
\((X, \topo)\) is called \underline{\(T_2\)} or \underline{Hausdorff} if
\[\forall x \neq y, \exists U, V \in \topo, U \cap V = \varnothing, x \in U, y \in V\]
\end{definition}
\begin{theorem}
In a Hausdorff space, sequences converge to at most one point.
\end{theorem}
An excellent question you can ask is ``does this implication reverse.'' The answer to this is no. As an example, \((\reals, \topo_{\text{co-countable}})\) is not \(T_2\), but the only sequences that converge are eventually constant, and the space is \(T_1\) implying these sequences converge to nothing else.


\end{document}