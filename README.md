My notes from MAT327 (LEC0101, Ivan Khatchatourian) in LaTeX. Warning: May be incomplete/incorrect!

You probably want to consult the lecturer's notes [here](http://www.math.toronto.edu/ivan/mat327/?resources) instead.

Licensed under the BSD 3-clause license. Please feel free to contribute! Compiled using ```latexmk --pdf```.

